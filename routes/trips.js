const express = require('express');
const router = express.Router();
const Trip = require('../models/Trip');

/* GET home page. */
router.get('/', function(req, res, next) {
    Trip.find((err, result) => {
        if (err) {
            console.error(err);
            return res.status(500).send(err);
        }
        res.render('trips', {trips: result});
    });
});

router.get('/get-trip/:id', (req, res, next) => {
    const tripId = req.params.id;
    Trip.findOne({_id: tripId}, (err, result) => {
        if (err) {
            return res.status(500).send(err);
        }
        res.status(200).send(result);
    });
});

router.get('/add', (req, res, next) => {
    res.render('add-trip');
});

router.post('/add', (req, res, next) => {
    const newTrip = new Trip(req.body);

    newTrip.save((err, result) => {
        if (err) {
            return res.status(500).send(err);
        }
        res.status(200).send(req.body);
    });
});

module.exports = router;
