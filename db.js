const mongoose = require('mongoose');
const Trip = require('./models/Trip');

module.exports = (dbUrl) => {
    mongoose.connect(`${dbUrl}/momencar`, (err, res) => {
        if (err) {
            return console.error(err);
        }
        console.log('connected to db');
    });
};
