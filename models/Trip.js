const mongoose = require('mongoose');

const TripSchema = {
    from: String,
    to: String,
    leaving: Date,
    driver: String,
    phone: String,
    email: String,
    spots: Number,
    other: String,
    subscribers: [String]
};

const Trip = mongoose.model('Trip', TripSchema);

module.exports = Trip;