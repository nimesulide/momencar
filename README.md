# MomenCar
An experiment to provide minimalistic carsharing service for the 'Momentum Tábor'.

### Prerequisites
- mongodb
- nodejs
- npm

### Installing
```
npm install
```

### Running
```
npm start
```